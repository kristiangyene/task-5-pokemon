import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from '../../models/pokemon.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent implements OnInit {

  @Input() pokemon: Pokemon; 

  constructor(private router: Router) { }

  ngOnInit(): void {    
  }

  goToDetails(){
    this.router.navigate(['/catalogue', this.pokemon.id]);
  }

}
