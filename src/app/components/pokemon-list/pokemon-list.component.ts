import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PokemonService } from '../../services/pokemon.service'
import { Pokemon } from '../../models/pokemon.model';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {
  private pokemon$: Subscription;
  public pokemonList: Pokemon[] = [];

  constructor(private pokemonService: PokemonService) {
    this.pokemon$ = this.pokemonService.pokemon$.subscribe((pokemonList: Pokemon[]) => {
      this.pokemonList = pokemonList;
    });
  }

  ngOnInit(): void {
    this.pokemonService.getPokemon();
  }
  ngOnDestroy(): void {
    this.pokemon$.unsubscribe();
  }
}
