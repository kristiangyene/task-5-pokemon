export interface Pokemon {
  id?: number;
  name: string;
  url: string;
  abilities?: any[];
  baseExperience?: number;
  forms?: any;
  moves?: any[];
  sprites?: Sprites;
  stats?: any[];
  height?: number;
  weight?: number;
  types?: any[];
}

interface Sprites {
    front_default: string;
    back_default: string;
    back_shiny: string;
    front_shiny: string;
}
  

