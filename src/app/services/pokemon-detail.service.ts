import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { Pokemon } from '../models/pokemon.model';
import {PokemonService} from './pokemon.service';

@Injectable({
  providedIn: 'root'
})
export class PokemonDetailService {

  public pokemon$: BehaviorSubject<Pokemon> = new BehaviorSubject(null);

  constructor(private http: HttpClient,
    private pokemonService: PokemonService) {
  }


  getPokemonById(id: number): void {
    this.pokemon$.next( null );
    this.http.get( `${ environment.apiBaseUrl }/pokemon/${ id }` ).subscribe( (pokemon: Pokemon) => {
      this.pokemon$.next( pokemon );
    } );
  }
}